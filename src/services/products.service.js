import axios from "axios";
import { apiConfig } from '../env.js';
import FormData from "form-data";
import fs from 'fs';



export const fetchProduct = async(reqProduct) => {
    try{
        return await axios.get(`${apiConfig.baseUrlAl_v1}/api/json/public/item/v2.do?countryCode=${reqProduct.countryCode}&languageCode=${reqProduct.languageCode}&areaId=${reqProduct.areaId}&categoryId=${reqProduct.categoryId ?? 0}&offset=${reqProduct.offset}&count=${reqProduct.count}&isSku=${reqProduct.isSku}&searchEngine=${reqProduct.searchEngine}&parentCategoryId=${reqProduct.parentCategoryId ?? 0}`)
    }
    catch(error){
        console.log(error);
    }
}

export const fetchProductDetail = async(reqProduct) => {
    try{
        return await axios.get(`${apiConfig.baseUrlAl_v1}/api/json/public/item/detail.do?id=${reqProduct.id}&selectedSkuId=${reqProduct.selectedSkuId}&countryCode=${reqProduct.countryCode}&languageCode=${reqProduct.languageCode}&selectedAreaId=${reqProduct.selectedAreaId}&deviceId=${reqProduct.deviceId}`)
    }
    catch(error){
        console.log(error);
    }
}

export const fetchProductOrderInfo = async(reqProduct) => {
    try{
        return await axios.post(`${apiConfig.baseUrlAl_v2}/capi/itemdetail/public/item/detail/order-statistics`, {
            spuId: reqProduct.spuId
        })
    }
    catch(error){
        console.log(error);
    }
}

export const fetchProductDescription = async(reqProduct) => {
    try{
        return await axios.post(`${apiConfig.baseUrlAl_v2}/capi/itemdetail/public/item/detail/desc`, {
            spuId: reqProduct.spuId,
            countryId: reqProduct.categoryId
        })
    }
    catch(error){
        console.log(error);
    }
}


export const fetchProductCommentList = async(reqProduct) => {
    try{
        return await axios.post(`${apiConfig.baseUrlAl_v2}/capi/itemdetail/public/item/detail/comment-list`, {
            spuId: reqProduct.spuId,
            countryId: reqProduct.categoryId
        })
    }
    catch(error){
        console.log(error);
    }
}

export const fetchProductCommentLabel = async(reqProduct) => {
    try{
        return await axios.post(`${apiConfig.baseUrlAl_v2}/capi/itemdetail/public/item/detail/comment-label`, {
            spuId: reqProduct.spuId,
            countryId: reqProduct.categoryId
        })
    }
    catch(error){
        console.log(error);
    }
}

export const fetchProductLink = async(reqProduct) => {
    try{
        const formData = new FormData();
        formData.append('content', `ak://m.akulaku.com/1301?id=${reqProduct.itemId}&countryCode=${reqProduct.countryCode}`);
        return await axios.post(`${apiConfig.baseUrlAl_v1}/api/json/public/jump/link/addOrSearch.do`, formData, {
            headers: formData.getHeaders()
        })
    }
    catch(error){
        console.log(error);
    }
}


