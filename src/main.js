import { apiConfig, fileStorageConfig } from './env.js';
import { fetchProduct, fetchProductDetail, 
  fetchProductDescription, fetchProductOrderInfo, 
  fetchProductCommentList, fetchProductLink, fetchProductCommentLabel } from './services/products.service.js';
import { prettyJson } from './utils/jsonFormatter.js';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { importToCsv } from './utils/jsonToCSV.js';

/* LIST OF MAIN CATEGORY ID
  - MAN FASHION: 193
  - WOMAN FASHION: 400
  - HOME & KITCHEN TOOLS: 389
  - OTHERS: 394,
  - GADGET & ACC: 281
  - HEALTH & BEAUTY: 360
*/

const categoryList = [
  193, 400, 389, 394, 280, 360
];

const onStartDataScraping = async () => {
  console.log('type CTRL + C to terminate job');
   let currentPage = 0;
   for(let index = 0; index < reqProduct.maxOffset; index++){
      let productList = [];
      reqProduct.offset = currentPage;
      let innerItemCount = 1;
      try{
         const products = await fetchProduct(reqProduct).then(response => response.data);
         if(products == null){ continue;}

         console.log(`Page: ${currentPage}, Total Item: ${products.data.list.length}`)
         
         for(let innerIndex = 0; innerIndex < products.data.list.length; innerIndex++){
             reqProductDetail.id = products.data.list[innerIndex].itemId;
             reqProductDetail.selectedSkuId = products.data.list[innerIndex].skuId ?? 0;

             reqOrderInfo.spuId = products.data.list[innerIndex].itemId;
             reqProductDesc.spuId = products.data.list[innerIndex].itemId;
             reqProductComment.spuId = products.data.list[innerIndex].itemId;
             reqProductLink.itemId = products.data.list[innerIndex].itemId;

             const details = await fetchProductDetail(reqProductDetail).then(response => { if(response.data != null){  return response.data; }});
             const orderInfo = await fetchProductOrderInfo(reqOrderInfo).then(response => { if(response.data != null){  return response.data; }});
             const description = await fetchProductDescription(reqProductDesc).then(response => { if(response.data != null){  return response.data; }});
             const userComments = await fetchProductCommentList(reqProductComment).then(response => { if(response.data != null){  return response.data; }});
            //  const commentLabel = await fetchProductCommentLabel(reqProductComment).then(response => { if(response.data != null){  return response.data; }});
             const links = await fetchProductLink(reqProductLink).then(response => { if(response.data != null){  return response.data; }});

            let currentProduct = {
                itemId: products.data.list[innerIndex].itemId,
                skuId: products.data.list[innerIndex].skuId,
                categoryId: details.data.catId,
                categoryName: details.data.catName,
                name: products.data.list[innerIndex].itemName,
                stock: details.data.stock,
                price: products.data.list[innerIndex].price,
                downPayment: products.data.list[innerIndex].downPayment,
                rating: products.data.list[innerIndex].score,
                saleCount: orderInfo.data.spuSaleCount,
                description: description.data.desc,
                merchantId:details.data.vendorId,
                merchantName:details.data.vendorName,
                userComments: userComments.data.spuCommentInfos != null ? JSON.stringify(userComments.data.spuCommentInfos) : [],
                // commentLabel: commentLabel.data.labels != null ? JSON.stringify(commentLabel.data.labels) : [],
                frontImage: products.data.list[innerIndex].img,
                bannerImgUrls: details.data.bannerImgUrls,
                descImgUrls: details.data.descImgUrls,
                isCod: products.data.list[innerIndex].isCod,
                tagContent: products.data.list[innerIndex].tagContent,
                skuAttributes: products.data.list[innerIndex].skuAttributes,
                link: links.data.resultActionJumpLink.actionKey,
             }
              
             productList.push(currentProduct);

             console.log(`${innerItemCount} of ${products.data.list.length}`)
             innerItemCount++;

             if(innerItemCount > products.data.list.length){
               console.log("Completed \u221A")
             }
         }
        
        // Import data to CSV
        //  await importToCsv(`${fileStorageConfig.csvPath}`,'akulaku-data.csv', fields, productList, false);
      }
      catch(error){
        console.log(error);
      }

      currentPage++;
      of(productList)
      .pipe(map((data) => data ))
      .subscribe((result) => console.log(prettyJson(result)));

   }
};

const fields = [
  { id: 'itemId', title: "ITEM ID" },
  { id: 'skuId', title: "SKU ID" },
  { id: 'categoryId', title: "CATEGORY ID" },
  { id: 'categoryName', title: "CATEGORYNAME" },
  { id: 'name', title: "ITEM NAME" },
  { id: 'stock', title: "STOCK" },
  { id: 'price', title: "PRICE" },
  { id: 'downPayment', title: "DOWNPAYMENT" },
  { id: 'rating', title: "RATING" },
  { id: 'saleCount', title: "SALE COUNT" },
  { id: 'description', title: "DESCRIPTION" },
  { id: 'merchantId', title: "MERCHANT ID" },
  { id: 'merchantName', title: "MERCHANT NAME" },
  { id: 'userComments', title: "USER COMMENTS" },
  // { id: 'commentLabel', title: "COMMENT LABELS" },
  { id: 'frontImage', title: "FRONT IMAGE" },
  { id: 'bannerImgUrls', title: "BANNER IMAGE" },
  { id: 'descImgUrls', title: "DESC IMAGE" },
  { id: 'isCod', title: "IS COD" },
  { id: 'tagContent', title: "TAG CONTENT" },
  { id: 'skuAttributes', title: "SKU ATTRIBUTES" },
  { id: 'link', title: "LINK" },
]

let reqProduct = {
    countryCode: apiConfig.countryCode,
    languageCode: apiConfig.languageCode,
    areaId:apiConfig.areaId, 
    categoryId:null, // product category
    offset:0, // pagination
    maxOffset: 9900, // last index of pagination
    count:100, // total item per page
    isSku:true,
    searchEngine:apiConfig.searchEngine,
    parentCategoryId:283 // base product category
}

let reqProductDetail = {
  id: 4474572,
  selectedSkuId:19584779, // product sku
  countryCode:apiConfig.countryCode, // indonesia = ID, malaysia = MY etc
  languageCode: apiConfig.languageCode,
  selectedAreaId:apiConfig.areaId,
  deviceId:apiConfig.deviceId // your device id, generated by app
}

let reqOrderInfo = {
  spuId: 4474572, // product identity (id)
}

let reqProductDesc = {
  spuId: 4474572, // product identity (id)
  countryId:1 // indonesia = 1
}

let reqProductComment = {
  spuId: 54142281, // product identity (id)
  countryId:1
}

let reqProductLink = {
  itemId: 54142281, // product identity (id)
  countryCode:apiConfig.countryCode
}


onStartDataScraping();