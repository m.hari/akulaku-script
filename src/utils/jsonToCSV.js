import { createObjectCsvWriter } from 'csv-writer';

export const importToCsv = async (path, fileName, header, data, isContinue = false) => {
    const createCsvWriter = createObjectCsvWriter;
    const csvWriter = createCsvWriter({
        path: `${path}/${fileName}`,
        header: header,
        append: isContinue
    });
    
    await csvWriter.writeRecords(data)       // returns a promise
        .then(() => {
            console.log('Successfully copied into file.');
    }).catch( (error) => {
        console.log(error);
    });
     
}

