import csv from 'csv-parser'
import { fs } from 'fs'

const initState = [];

fs.createReadStream(`${fileStorageConfig.csvPath}/checkpoint.csv`)
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
    console.log(results);
  });