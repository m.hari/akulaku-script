export const apiConfig = {
    // main host
    baseUrlAl_v1: "https://id-app.akulaku.com",
    baseUrlAl_v2: "https://ec-api.akulaku.com",
    baseUrlAl_mall: "https://ec-mall.akulaku.com",

    // env req
    deviceId: "00000000-63f4-c0a2-ffff-fffff8d0e35f",
    languageCode:"IN",
    languageId:"I23",
    countryCode:"ID",
    areaId:1,
    searchEngine: "es",
    deviceModel: "G011A",
    deviceType:4,
    uid:143130512,
    // static
    deviceToken:"ecv4jsykpcA:APA91bFzEZP1lR5QPD0hZ0NhlCQD6mwpi2EM1wikacMOW0mmz60pFkr78I8vWIDNM",
    captchaModel:"90501",
    reportInfo: "eyJkdCI6NCwiZGV2aWNlSWQiOiIwMDAwMDAwMC02M2Y0LWMwYTItZmZmZi1mZmZmZjhkMGUzNWYiLCJkZXZpY2VNb2RlbCI6IkcwMTFBIiwiZGV2aWNlQnJhbmQiOiJnb29nbGUiLCJpbWVpIjoiNjI4MzcxNjc4MDE5MDQwIiwiYWRJZCI6Ijg0NzBlYzZiLWUwYjktNDJjMi04ZjkzLWEwOGM0ZGE4YzA5ZCIsImFkanVzdElkIjoiMTgxOWQzNmI2NGZkNGFmYTk4MTYzZTM5NzQ3YzJhMjYiLCJvc1ZlcnNpb24iOiI3LjEuMiIsImFwcE5hbWUiOiJBa3VsYWt1IiwiYXBwUGFja2FnZU5hbWUiOiJpby5zaWx2cnIuaW5zdGFsbG1lbnQiLCJhcHBWZXJzaW9uIjoiMy4wLjgyIiwibmV0d29ya1R5cGUiOjEsIndpZmlNYWMiOiJkNDowMjowNjo0NTo0ZjozZiIsImxhdGl0dWRlIjozMS4yNDkxNiwibG9uZ2l0dWRlIjoxMjEuNDg3ODk4MywiZXh0ZW5kRmllbGRKc29uIjoie1wiYWNjZWxlcmF0aW9uXCI6XCIwLjAsOC45LDQuNVwiLFwiYmF0dGVyeVwiOjEwMCxcImJyYW5kXCI6XCJnb29nbGVcIixcImNhcnJpZXJfb3BlcmF0b3JcIjpcIlRlbGtvbXNlbFwiLFwiY2hhcmdlX3N0YXRlXCI6MSxcImRlbnNpdHlcIjoxLjUsXCJkaXJlY3Rpb25cIjpcIjAuMCw4LjksNC41XCIsXCJkaXN0YW5jZVwiOlwiMC4wLDguOSw0LjVcIixcImV0aF9pcFwiOlwiMTAuMC4yLjE1XCIsXCJncmF2aXR5XCI6XCIwLjAsOC45LDQuNVwiLFwiZ3lyb3Njb3BlXCI6XCIwLjAsOC45LDQuNVwiLFwiaGFyZF9kaXNrX2ZyZWVcIjoxMTEyNixcImhhcmRfZGlza190b3RhbFwiOjEyMzkxLFwiaGVpZ2h0XCI6MTI4MC4wLFwiaXNfYmx1ZXRvb3RoXCI6MSxcImlzX2VtdWxhdG9yXCI6MSxcImlzX2xvY2F0aW9uX2dwc1wiOjEsXCJpc19uZmNcIjowLFwiaXNfbmZjX2hvc3RfY2FyZF9lbXVsYXRpb25cIjowLFwiaXNfcm9vdGVkXCI6MSxcImlzX3RlbGVwaG9ueVwiOjEsXCJpc191c2JfaG9zdFwiOjEsXCJpc191c2VfYWNjZXNzb3J5XCI6MCxcImlzX3dpZmlcIjoxLFwiaXNfd2lmaV9kaXJlY3RcIjowLFwiamF2YV92aXJ0dWFsX21lbW9yeV9tYXhcIjoyNjg0MzU0NTYsXCJqYXZhX3ZpcnR1YWxfbWVtb3J5X3RvdGFsXCI6ODg0NDY0MCxcIm1hZ25ldGljX2ZvcmNlXCI6XCIwLjAsOC45LDQuNVwiLFwicGhvbmVfbnVtYmVyXCI6bnVsbCxcInNpbV9zdGF0ZVwiOjUsXCJzaW1fc3RyZW5ndGhcIjowLFwid2lkdGhcIjo3MjAuMH0ifQ=="
}

export const fileStorageConfig = {
    csvPath: "src/assets/csv/",
    statePath: "src/assets/temp/"
}

const buildConfig = {
    host:"localhost",
    port:"3100"
}

